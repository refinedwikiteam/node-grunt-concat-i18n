#!/usr/bin/env bash
if [ -z "$1" ]; then
    echo "Usage: npm publish {version type}"
    npm version --help
    exit 1;
else
    npm version $1 && git push --follow-tags && npm publish
fi
