module.exports = function(grunt) {
  'use strict';
  var fs = require('fs-extra');
  var util = require('util');
  var propertiesReader = require('properties-reader');
  var tmp = require('tmp');
  var iconv = require('iconv-lite');

  grunt.registerMultiTask('translations',
    'Concatenates and generates JavaScript friendly versions of i18n translations',
    function() {
      var requiredProperties = ['src', 'dest', 'scope'];
      var TEMP_DIR = tmp.dirSync();
      var config = grunt.config.get(this.name)[this.target];

      if (!config) {
        grunt.fail.fatal('Missing config');
      }

      for (let i = 0; i < requiredProperties.length; i++) {
        if (!config[requiredProperties[i]]) {
          grunt.fail.fatal('Missing required property ' + requiredProperties[i] + ' in config');
        }
      }

      if (Object.prototype.toString.call(config.src) !== '[object Array]') {
        grunt.fail.fatal('src must be an array');
      }

      if (!config.propertiesDest) {
        grunt.log.writeln('No destination specified for intermediate properties file, using temp dir');
        config.propertiesDest = TEMP_DIR;
      }

      let gruntDone = this.async();
      let concats = 0;
      let numFiles = 0;

      function getMissingPropsAsString(properties, defaultProperties) {
        const propertiesKeys = Object.keys(properties);
        const defaultLanguageKeys = Object.keys(defaultProperties);

        // Find properties that are missing and add them from the default language
        let missingProperties = '';
        defaultLanguageKeys.filter((defaultProperty) => {
            return !propertiesKeys.includes(defaultProperty);
          })
          .map((missingProperty) => {
            missingProperties += missingProperty + " = " + defaultProperties[missingProperty] + "\n";
          });
        return missingProperties;
      }

      function makeJSONTranslations(content, language) {
        var preparedContent = content.replace(/\\u([0-9a-fA-F]{4})/g, (m, cc)=>String.fromCharCode("0x" + cc));
        var tempOutput = config.propertiesDest + 'temporary' + (language.length > 0 ? '_' + language : '') + '.properties';
        fs.writeFileSync(tempOutput, preparedContent);
        let output = 'var translations = {};';
        var properties = propertiesReader(tempOutput);
        var allProperties = properties.getAllProperties();
        fs.mkdirsSync(config.dest);
        if (!config.webpack) {

          var scopeInit = 'if(' + config.scope+' === undefined) {\n'+config.scope+' = {};\n}\n'+
            'var keys = ' + util.inspect(allProperties, {depth: null}) + ';\n' +
            'for(var k in keys) {\n'+config.scope+'[k]=keys[k];\n}';

          fs.writeFileSync(config.dest + 'translations' + (language.length > 0 ? '_' + language : '') +
            '.js',
            scopeInit, 'utf8');
        }
        output += '\n translations[\'' + (language.length > 0 ? language : '') + '\'] = ' +
          util.inspect(allProperties, {depth: null});
        output += '\nmodule.exports = translations;';
        fs.writeFileSync(config.dest + 'translations-require.js', output);
        TEMP_DIR.removeCallback();
        fs.unlinkSync(tempOutput);
        grunt.log.ok('Wrote translations in JS format for ' + language);
      }

      fs.mkdirsSync(config.propertiesDest);
      fs.mkdirsSync(config.dest);
      let languages = config.languages;
      for (let i = 0; i < languages.length; i++) {
        var language = languages[i];
        var output = config.propertiesDest + 'super' + (language.length > 0 ? '_' + language : '') + '.properties';
        concats++;
        let localSrc = config.src.slice(0);
        for (let j = 0; j < localSrc.length; j++) {
          const src = localSrc[j];
          localSrc[j] = src + (language.length > 0 ? '_' + language : '') + '.properties';

          // If the file doesn't exist use the selected default language instead
          if (config.defaultLanguage !== undefined && !fs.existsSync(localSrc[j])) {
            localSrc[j] = src + (config.defaultLanguage.length > 0 ? '_' + config.defaultLanguage : '') + '.properties';
          }
        }
        let concat = '';
        for (let j = 0; j < localSrc.length; j++) {
          try {
            let content = fs.readFileSync(localSrc[j]);
            // This will parse the string  and replace our double '' for strings
            concat += iconv.decode(content, 'utf8');

            if (config.defaultLanguage !== undefined && language !== config.defaultLanguage) {
              var defaultProperties = propertiesReader(localSrc[j].replace('_' + language, config.defaultLanguage.length > 0 ? '_' + config.defaultLanguage : ''));
              var properties = propertiesReader(localSrc[j]);
              concat += getMissingPropsAsString(properties.getAllProperties(), defaultProperties.getAllProperties());
            }
          } catch (err) {
            if (err.code === 'ENOENT') {
              grunt.log.writeln(err.message['yellow']);
            }
            else {
              grunt.fail.fatal(err);
            }
          }
        }

        makeJSONTranslations(concat, language);
        fs.writeFileSync(output, concat);
        numFiles++;
      }
      grunt.log.ok((numFiles + 1) + ' translation files written.');
      gruntDone();
    });
};
