# grunt-concat-18n

A Grunt task for concatenating i18n files and generating a JavaSript friendly version of the result. 

## Getting Started
This plugin requires Grunt `>=1.5.3`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-concat-i18n --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-concat-i18n');
```

## Usage

```js
require('load-grunt-tasks')(grunt); // npm install --save-dev load-grunt-tasks

grunt.initConfig({
    'translations': {
         frontend: {
           dest: 'public/gen/js',
           languages: ['', 'de_DE', 'es_ES', 'fr_FR', 'nl_NL'],
           src: ['path/to/properties', 'other/path/to/properties'],
           scope: 'My.own.JS.scope',
           propertiesDest: 'public/gen/properties',
         }
    }
});

grunt.registerTask('default', ['translations']);
```

### Required Options

#### dest
Type: `String`

The final destination for the JavaScript files

#### languages
Type: `Array`

The list of language codes.

#### src
Type `Array`

The list of properties bundle names without language codes and file endings

#### scope
Type `String`

The scope on which the generated JS files will be put. For use with for example [localized-string](https://www.npmjs.com/package/localized-string) enter `module.exports`

#### propertiesDest
Type: `String`

Default: Temporary directory`

Specified the output directory for the intermediate, concatenated, properties file
